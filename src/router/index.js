import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Jupiter from '../views/Jupiter.vue'
import DrinksOnMe from '../views/DrinksOnMe.vue'
import Drift from '../views/Drift.vue'
import PIMSSafety from '../views/PIMSSafety.vue'
import PIMSMobile from '../views/PIMSMobile.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/jupiter',
    name: 'Jupiter',
    component: Jupiter
  },
  {
    path: '/drinks-on-me',
    name: 'DrinksOnMe',
    component: DrinksOnMe
  },
  {
    path: '/drift',
    name: 'Drift',
    component: Drift
  },
  {
    path: '/pims-safety',
    name: 'PIMSSafety',
    component: PIMSSafety
  },
  {
    path: '/pims-mobile',
    name: 'PIMSMobile',
    component: PIMSMobile
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

export default router
